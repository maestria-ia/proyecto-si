# Comparacion de modelos de inteligencia artificial en la prediccion de ventas 

## Descripcion: 

El codigo expuesto en este repositorio es parte de un proyecto academico cuyo objetivo es la comparacion de diferentes modelos de inteligencia artificial aplicados en la solucion de un problema (prediccion de ventas). La prediccion de ventas es un tema de gran controversia e investigacion en la actualidad, dado que de entrenar un modelo preciso, una compañia estaria en capacidad de ahorrar grandes costos financieros, ademas de tomar una gran ventaja frente a su competencia. 

## Instalacion y ejecucion del proyecto:

Dado que los resultados compartidos en este repositorio han sido desarrollados en [Google Colab](https://colab.research.google.com), no se hace necesario ningun tipo de instalacion con el fin de hacer uso de los Notebooks expuestos, de igual forma, los conjuntos de datos utilizados a lo largo de la implementacion han sido dispuestos en URLs de acceso publico, por lo que de ser necesario, los interesados en analizar la informacion pueden hacerlo facilmente haciendo uso de los links compartidos a continuacion:

- Transacciones: https://drive.google.com/file/d/1FPJOyewve7mer35a4nYjehhZF1CU8ho6/view?usp=sharing
- Tiendas: https://drive.google.com/file/d/1FPJOyewve7mer35a4nYjehhZF1CU8ho6/view?usp=sharing
- Categorias: https://drive.google.com/file/d/1FPJOyewve7mer35a4nYjehhZF1CU8ho6/view?usp=sharing
- Productos: https://drive.google.com/file/d/1FPJOyewve7mer35a4nYjehhZF1CU8ho6/view?usp=sharing

## Estructura del repositorio:


    .
    ├── data_exploration.ipynb            # Data exploration and engineering Notebook 
    ├── models                            # Models trained
    └── README.md


## Creditos:

- Jorge Luis Quintero Medina (Desarrollador): https://github.com/JorgeQuinteroM
- Diego Alejandro Mendoza (Desarrollador): https://github.com/derdiego91





